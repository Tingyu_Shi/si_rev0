import pygame

class Block(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.Surface((5, 5))
        self.image.fill((247, 59, 23))
        self.rect = self.image.get_rect(topleft = (x, y))


class Obstacles(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()

        self.blocks_group = pygame.sprite.Group()

        self.create_one_obstacle(50, 400)
        self.create_one_obstacle(250, 400)
        self.create_one_obstacle(450, 400)
        self.create_one_obstacle(650, 400)

    def create_one_obstacle(self, x_start, y_start):
        for row in range(10):
            for col in range(17):
                x = x_start + col * 5
                y = y_start + row * 5
                block = Block(x , y)
                self.blocks_group.add(block)
    
    def getBlocksGroup(self):
        return self.blocks_group
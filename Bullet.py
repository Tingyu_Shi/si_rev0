import pygame

class Bullet(pygame.sprite.Sprite):
    #direction == 1   flying up
    def __init__ (self, x, y, screen_size_info, speed, direction):
        super().__init__()

        self.screen_size_info = screen_size_info
        self.speed = speed
        if direction == 1:
            self.image = pygame.image.load('Picture/bullet.png')
            self.image = pygame.transform.scale(self.image, (20, 20))
        else:
            self.image = pygame.image.load('Picture/bullet_down.png')
            self.image = pygame.transform.scale(self.image, (20, 20))
        
        self.rect = self.image.get_rect(center = (x, y))
    def boundaryDetection(self):
        if self.rect.y <= -30 or self.rect.y >= self.screen_size_info[1]:
            self.kill()
    
    def move(self):
        self.rect.y += self.speed
    
    def update(self):
        self.move()
        self.boundaryDetection()
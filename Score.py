class Score:
    def __init__(self):
        self.s = 0
    
    def increase(self, amount):
        self.s += amount

    def getScore(self):
        return self.s
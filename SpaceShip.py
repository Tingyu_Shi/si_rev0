import pygame
from Bullet import *
from BulletState import *

class SpaceShip(pygame.sprite.Sprite):
    # screen_size_info is a tuple (width, height)
    def __init__(self, x, y, screen_size_info, speed, space_ship_number):
        super().__init__()
        
        self.screen_size_info = screen_size_info
        self.space_ship_number = space_ship_number
        if space_ship_number == 1:
            self.image = pygame.image.load('Picture/spaceship1.png')
        else:
            self.image = pygame.image.load('Picture/spaceship2.png')
        self.rect = self.image.get_rect(midbottom = (x, y))
        self.speed = speed
        self.life = 5

        # bullets information
        self.bullets_group = pygame.sprite.Group()
        self.state = BulletState.READY
        self.shoot_time = 0
        self.bullet_number = 1

    def move(self):
        keys = pygame.key.get_pressed()

        if self.space_ship_number == 1:
            if keys[pygame.K_RIGHT]:
                self.rect.x += self.speed
            if keys[pygame.K_LEFT]:
                self.rect.x -= self.speed
        else:
            if keys[pygame.K_d]:
                self.rect.x += self.speed
            if keys[pygame.K_a]:
                self.rect.x -= self.speed
    
    def boundaryDetection(self):
        if self.rect.left <= 0:
            self.rect.left = 0
        if self.rect.right >= self.screen_size_info[0]:
            self.rect.right = self.screen_size_info[0]

    def shoot(self):
        bullet_speed = -8  ########
        keys = pygame.key.get_pressed()
        if self.space_ship_number == 1:
            if keys[pygame.K_SPACE] and self.state == BulletState.READY:
                if self.bullet_number == 1:
                    self.bullets_group.add(Bullet(self.rect.center[0], self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.state = BulletState.FIRE
                    self.shoot_time = pygame.time.get_ticks()
                if self.bullet_number == 2:
                    self.bullets_group.add(Bullet(self.rect.center[0] - 25, self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.bullets_group.add(Bullet(self.rect.center[0] + 25, self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.state = BulletState.FIRE
                    self.shoot_time = pygame.time.get_ticks()
                if self.bullet_number == 3:
                    self.bullets_group.add(Bullet(self.rect.center[0] - 25, self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.bullets_group.add(Bullet(self.rect.center[0], self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.bullets_group.add(Bullet(self.rect.center[0] + 25, self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.state = BulletState.FIRE
                    self.shoot_time = pygame.time.get_ticks()

        else:
            if keys[pygame.K_s] and self.state == BulletState.READY:
                if self.bullet_number == 1:
                    self.bullets_group.add(Bullet(self.rect.center[0], self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.state = BulletState.FIRE
                    self.shoot_time = pygame.time.get_ticks()
                if self.bullet_number == 2:
                    self.bullets_group.add(Bullet(self.rect.center[0] - 25, self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.bullets_group.add(Bullet(self.rect.center[0] + 25, self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.state = BulletState.FIRE
                    self.shoot_time = pygame.time.get_ticks()
                if self.bullet_number == 3:
                    self.bullets_group.add(Bullet(self.rect.center[0] - 25, self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.bullets_group.add(Bullet(self.rect.center[0], self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.bullets_group.add(Bullet(self.rect.center[0] + 25, self.rect.center[1], (800, 600), bullet_speed, 1))  
                    self.state = BulletState.FIRE
                    self.shoot_time = pygame.time.get_ticks()


    def prepare_bullet(self):
        delay_time = 500  ##########
        if self.state == BulletState.FIRE:
            if pygame.time.get_ticks() - self.shoot_time >= delay_time:   ###
                self.state = BulletState.READY

    def getBulletsGroup(self):
        return self.bullets_group

    def update(self):
        self.move()
        self.boundaryDetection()
        self.shoot()
        self.prepare_bullet()
        self.bullets_group.update()

    def reduceLife(self):
        self.life -= 1
    
    def increaseLift(self):
        self.life += 1
        if self.life >= 5:
            self.life = 5

    def getLife(self):
        return self.life
    
    def setLift(self, num):
        self.life = num
    
    def increaseBullet(self):
        self.bullet_number += 1
        if self.bullet_number >= 3:
            self.bullet_number = 3

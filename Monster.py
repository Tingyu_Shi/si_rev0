import pygame
from MonsterColor import *

class Monster(pygame.sprite.Sprite):
    def __init__(self, x, y, color, speed):
        super().__init__()
        if (color == MonsterColor.GREEN):
            filepath = 'Picture/green_monster.png'
            self.life = 1
        if (color == MonsterColor.BLUE):
            filepath = 'Picture/blue_monster.png'
            self.life = 2
        if (color == MonsterColor.PINK):
            filepath = 'Picture/pink_monster.png'
            self.life = 3

        self.speed = speed
        self.image = pygame.image.load(filepath)
        self.image = pygame.transform.scale(self.image, (30, 30))
        self.rect = self.image.get_rect(topleft = (x, y))

    def reduceLife(self):
        self.life -= 1
    
    def isDead(self):
        return self.life == 0

    # direction can only be 1 or -1
    def update(self, direction):
        self.rect.x += (direction * self.speed)
    
    def getItemType(self):
        return 1
    
        
import pygame

class Bomb(pygame.sprite.Sprite):
    def __init__(self, x, y, speed):
        super().__init__()
        self.speed = speed
        self.image = pygame.image.load('Picture/Bomb.png')
        self.image = pygame.transform.scale(self.image, (30, 30))
        self.rect = self.image.get_rect(topleft = (x, y))

    # direction can only be 1 or -1
    def update(self, direction):
        self.rect.x += (direction * self.speed)
    
    def getItemType(self):
        return 4
    
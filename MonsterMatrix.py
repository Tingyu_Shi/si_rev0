import pygame
from Monster import *
from MonsterColor import *
from Bullet import *
from random import choice
from random import randint
from Heart import *
from Ammo import *
from Bomb import *

class MonsterMatrix(pygame.sprite.Sprite):
    def __init__(self, round, speed, screen_size_info):
        super().__init__()
        self.monsters_group = pygame.sprite.Group()
        self.speed = speed
        self.direction = 1
        self.screen_size_info = screen_size_info

        if round == 1:
            self.round1()
        if round == 2:
            self.round2()
        if round == 3:
            self.round3()
        if round == 4:
            self.round4()
        if round == 5:
            self.round5()

        self.bullets_group = pygame.sprite.Group()

    def round1(self):
        itme_type = randint(2, 4)
        ran_row = randint(0, 4)
        ran_col = randint(0, 9)
        for row in range(5):
            for col in range(10):
                x = col * 50
                y = 50 + row * 30
                if row == ran_row and col == ran_col:
                    if itme_type == 2:
                        monster = Heart(x, y, self.speed)
                    if itme_type == 3:
                        monster = Ammo(x, y, self.speed)
                    if itme_type == 4:
                        monster = Bomb(x, y, self.speed)
                else:    
                    monster = Monster(x, y, MonsterColor.GREEN, self.speed)        
                self.monsters_group.add(monster)

    def round2(self):
        itme_type = randint(2, 4)
        ran_row = randint(0, 4)
        ran_col = randint(0, 9)
        for row in range(5):
            for col in range(10):
                x = col * 50
                y = 50 + row * 30
                if row == ran_row and col == ran_col and itme_type == 2:
                    monster = Heart(x, y, self.speed)
                elif row == ran_row and col == ran_col and itme_type == 3:
                    monster = Ammo(x, y, self.speed)
                elif row == ran_row and col == ran_col and itme_type == 4:
                    monster = Bomb(x, y, self.speed)
                elif row == 0 or row == 1:
                    color = MonsterColor.BLUE
                    monster = Monster(x, y, color, self.speed)
                else:
                    color = MonsterColor.GREEN
                    monster = Monster(x, y, color, self.speed)
                self.monsters_group.add(monster)

    def round3(self):
        itme_type = randint(2, 4)
        ran_row = randint(0, 4)
        ran_col = randint(0, 9)
        for row in range(5):
            for col in range(10):
                x = col * 50
                y = 50 + row * 30
                if row == ran_row and col == ran_col:
                    if itme_type == 2:
                        monster = Heart(x, y, self.speed)
                    if itme_type == 3:
                        monster = Ammo(x, y, self.speed)
                    if itme_type == 4:
                        monster = Bomb(x, y, self.speed)
                else:
                    monster = Monster(x, y, MonsterColor.BLUE, self.speed)
                self.monsters_group.add(monster)

    def round4(self):
        itme_type = randint(2, 4)
        ran_row = randint(0, 4)
        ran_col = randint(0, 9)
        for row in range(5):
            for col in range(10):
                x = col * 50
                y = 50 + row * 30
                if row == ran_row and col == ran_col and itme_type == 2:
                    monster = Heart(x, y, self.speed)
                elif row == ran_row and col == ran_col and itme_type == 3:
                    monster = Ammo(x, y, self.speed)
                elif row == ran_row and col == ran_col and itme_type == 4:
                    monster = Bomb(x, y, self.speed)
                elif row == 4:
                    color = MonsterColor.GREEN
                    monster = Monster(x, y, color, self.speed)
                elif row == 3 or row == 2 or row == 1:
                    color = MonsterColor.BLUE
                    monster = Monster(x, y, color, self.speed)
                else:
                    color = MonsterColor.PINK
                    monster = Monster(x, y, color, self.speed)
                self.monsters_group.add(monster)

    def round5(self):
        itme_type = randint(2, 4)
        ran_row = randint(0, 4)
        ran_col = randint(0, 9)
        for row in range(5):
            for col in range(10):
                x = col * 50
                y = 50 + row * 30
                if row == ran_row and col == ran_col:
                    if itme_type == 2:
                        monster = Heart(x, y, self.speed)
                    if itme_type == 3:
                        monster = Ammo(x, y, self.speed)
                    if itme_type == 4:
                        monster = Bomb(x, y, self.speed)
                else:
                    monster = Monster(x, y, MonsterColor.PINK, self.speed)
                self.monsters_group.add(monster)

    def getMonstersGroup(self):
        return self.monsters_group

    def boundaryDetection(self):
        allMonsters = self.monsters_group.sprites()
        for ele in allMonsters:
            if ele.rect.right >= self.screen_size_info[0]:
                self.direction = -1
                self.move_down()
            if ele.rect.left <= 0:
                self.direction = 1
                self.move_down()

    def move_down(self):
        if self.monsters_group:
            for ele in self.monsters_group.sprites():
                ele.rect.y += 2

    def shoot(self):
        temp_list = []
        if self.monsters_group.sprites():
            for monster in self.monsters_group.sprites():
                if monster.getItemType() == 1:
                    temp_list.append(monster)
            if (len(temp_list) != 0):
                random_monster = choice(temp_list)
                self.bullets_group.add(Bullet(random_monster.rect.center[0], random_monster.rect.center[1], (800, 600), 4, 2))

    def getBulletsGroup(self):
        return self.bullets_group

    def update(self):
        for ele in self.monsters_group.sprites():
            ele.update(self.direction)
        self.boundaryDetection()
        self.bullets_group.update()
from enum import Enum

class MonsterColor(Enum):
    GREEN = 1
    BLUE = 2
    PINK = 3
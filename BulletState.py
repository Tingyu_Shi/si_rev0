from enum import Enum

class BulletState(Enum):
    READY = 1
    FIRE = 2

